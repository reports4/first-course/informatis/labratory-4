import codecs

with codecs.open('schedule.json', "r", "utf-8") as inf:
    raw_data = inf.read().replace('\n', '').replace('  ', '')
    
json_parse = eval(raw_data)

strout = ""
for k, v in json_parse.items():
    strout += k + ": \n"
    for k1,v1 in v.items():
        strout += ' ' + k1 + ": \n"
        for k2,v2 in v1.items():
            strout += '  '+k2+':' + "\n"
            for k3,v3 in v2.items():
                strout += '   ' + k3 + ': ' + v3 + "\n"

f = codecs.open('test.yml', encoding='utf-8', mode='w+')
f.write(strout)
f.seek(0)
f.close()