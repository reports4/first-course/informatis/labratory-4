import codecs

with codecs.open('schedule.json', "r", "utf-8") as inf:
    raw_data = inf.read()

f = codecs.open('test1.yml', encoding='utf-8', mode='w+')
f.write(raw_data.replace('",', '').replace('\"', '').replace('{', '').replace('},', '').replace('}', ''))
f.seek(0)
f.close()

